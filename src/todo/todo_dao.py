class TodoDAO:
    def __init__(self, api):
        self.counter = 0
        self.todos = []
        self.api = api  # this sucks

    def get(self, id):
        for todo in self.todos:
            if todo['id'] == id:
                return todo
        self.api.abort(404, f"Todo {id} doesn't exist.")
        # return None

    def create(self, data):
        todo = data
        todo['id'] = self.counter = self.counter + 1
        self.todos.append(todo)
        return todo

    def update(self, id, data):
        todo = self.get(id)
        todo.update(data)
        return todo

    def delete(self, id):
        todo = self.get(id)
        self.todos.remove(todo)
